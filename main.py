import pandas as pd
from portfolio_performance import Portfolio


if __name__ == "__main__":
    currencies = pd.read_csv("./data/currencies.csv", index_col=0)
    exchanges = pd.read_csv("./data/exchanges.csv", index_col=0, parse_dates=[0])
    prices = pd.read_csv("./data/prices.csv", index_col=0, parse_dates=[0])
    weights = pd.read_csv("./data/weights.csv", index_col=0, parse_dates=[0])

    portfolio = Portfolio(currencies, exchanges, prices, weights)
    start = pd.Timestamp("2014-01-13")
    end = pd.Timestamp("2015-01-13")

    print(portfolio.calculate_asset_performance(start, end).iloc[-1])
    print(portfolio.calculate_currency_performance(start, end).iloc[-1])
    print(portfolio.calculate_total_performance(start, end).iloc[-1])
