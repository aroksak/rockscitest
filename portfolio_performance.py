import pandas as pd


class Portfolio(object):
    """
    Portfolio object allows to calculate portfolio performance based on
    prices, exchange rates and assert weights data.
    """

    def __init__(self, currencies: pd.DataFrame, exchanges: pd.DataFrame, prices: pd.DataFrame, weights: pd.DataFrame):
        """
        Create Portfolio object from currencies, exchange rates, prices and weights data.

        :param currencies: DataFrame with single column describing currencies used for each of the portfolio asserts,
            names of asserts must be DataFrame's index.
        :param exchanges: DataFrame with DatetimeIndex. Each column contains exchange rates for a currency.
        :param prices: DataFrame with DatetimeIndex. Each column corresponds to one of the portfolio asserts and
            contains its daily pricing in corresponding currency listed in currencies.
        :param weights: DataFrame with DatetimeIndex. Each column corresponds to one of the portfolio asserts and
            contains its weight in the whole portfolio.
        """
        exchanges = exchanges.resample('d').pad()
        exchanges = exchanges.fillna(method="pad", axis=0).fillna(method="bfill", axis=0)
        exchanges["USD"] = 1.0

        prices = prices.resample('d').pad()
        prices.fillna(method="pad", axis=0, inplace=True)

        weights = weights.resample('d').pad()

        prices_usd = pd.DataFrame()
        for col in prices.columns:
            prices_usd[col] = prices[col] * exchanges[currencies.loc[col, "currency"]]

        r_i = (prices - prices.shift(1)) / prices.shift(1)
        r_i.dropna(axis=0, inplace=True)

        cr_i = pd.DataFrame()
        for col in prices.columns:
            cur_col = currencies.loc[col, "currency"]
            cr_i[col] = (exchanges[cur_col] - exchanges[cur_col].shift(1)) / exchanges[cur_col].shift(1)
        cr_i.dropna(axis=0, inplace=True)

        tr_i = (prices_usd - prices_usd.shift(1)) / prices_usd.shift(1)
        tr_i.dropna(axis=0, inplace=True)

        self.R = (r_i * weights).sum(axis=1, skipna=False).dropna()
        self.CR = (cr_i * weights).sum(axis=1, skipna=False).dropna()
        self.TR = (tr_i * weights).sum(axis=1, skipna=False).dropna()

    def calculate_asset_performance(self, start_date: pd.Timestamp, end_date: pd.Timestamp) -> pd.Series:
        """
        Return Series of asset performances for period [start_date, end_date].

        Performance for start_date is 1.0 and for next days P_t = P_(t-1)*(1+R_t), where R_t is a weighted sum
        of returns of all portfolio's assets, each of them calculated like this: r_t = (p_t - p_(t-1)) / p_(t-1), where
        p_t is asset price at date t.

        :param start_date: Timestamp with the first day of the period.
        :param end_date: Timestamp with the last day of the period.
        :return: Series of asset performances with DatetimeIndex.
        """
        second_day = start_date + pd.Timedelta("1d")
        interesting_period = pd.Series({start_date: 1.0}).append(self.R.loc[second_day:end_date] + 1)
        return interesting_period.cumprod()

    def calculate_currency_performance(self, start_date: pd.Timestamp, end_date: pd.Timestamp) -> pd.Series:
        """
        Return Series of currency portfolio performances for period [start_date, end_date].

        Performance for start_date is 1.0 and for next days CP_t = CP_(t-1)*(1+CR_t), where CR_t is a weighted sum
        of currency returns of all portfolio's assets, each of them calculated like this:
        cr_t = (c_t - c_(t-1)) / c_(t-1), where c_t is asset currency exchange rate at date t.

        :param start_date: Timestamp with the first day of the period.
        :param end_date: Timestamp with the last day of the period.
        :return: Series of currency performances with DatetimeIndex.
        """
        second_day = start_date + pd.Timedelta("1d")
        interesting_period = pd.Series({start_date: 1.0}).append(self.CR.loc[second_day:end_date] + 1)
        return interesting_period.cumprod()

    def calculate_total_performance(self, start_date: pd.Timestamp, end_date: pd.Timestamp) -> pd.Series:
        """
        Return Series of total portfolio performances for period [start_date, end_date].

        Performance for start_date is 1.0 and for next days TP_t = TP_(t-1)*(1+TR_t), where TR_t is a weighted sum
        of total returns of all portfolio's assets, each of them calculated like this:
        tr_t = (c_t*p_t - c_(t-1)*p_(t-1)) / (c_(t-1)*p_(t-1)), where c_t is asset currency exchange rate at date t and
        p_t is asset price at date t.

        :param start_date: Timestamp with the first day of the period.
        :param end_date: Timestamp with the last day of the period.
        :return: Series of total performances with DatetimeIndex.
        """
        second_day = start_date + pd.Timedelta("1d")
        interesting_period = pd.Series({start_date: 1.0}).append(self.TR.loc[second_day:end_date] + 1)
        return interesting_period.cumprod()
