import unittest
from portfolio_performance import Portfolio
import pandas as pd


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        cur = pd.read_csv("./data/currencies.csv", index_col=0)
        exc = pd.read_csv("./data/exchanges.csv", index_col=0, parse_dates=[0])
        pri = pd.read_csv("./data/prices.csv", index_col=0, parse_dates=[0])
        wei = pd.read_csv("./data/weights.csv", index_col=0, parse_dates=[0])
        self.portfolio = Portfolio(cur, exc, pri, wei)

    def test_assert_performance(self):
        start, end = pd.Timestamp("2014-01-13"), pd.Timestamp("2014-01-14")
        self.assertEqual(
            1 + (0.03/14.27*0.275706352908 + (-0.31)/60.75*0.0428412771861 +
                 1.07/51.57*0.303424689127 + 0.2/35.2*0.193368025181),
            self.portfolio.calculate_asset_performance(start, end).iloc[-1]
        )

    def test_currency_performance(self):
        start, end = pd.Timestamp("2014-01-13"), pd.Timestamp("2014-01-14")
        self.assertEqual(
            1. + (-0.00185) / 1.36335 * (0.275706352908 + 0.0428412771861),
            self.portfolio.calculate_currency_performance(start, end).iloc[-1]
        )

    def test_total_performance(self):
        start, end = pd.Timestamp("2014-01-13"), pd.Timestamp("2014-01-14")
        # tr_AT = 0
        tr_BE = (14.3*1.3615 - 14.27*1.36335) / (14.27*1.36335)
        tr_DE = (60.44*1.3615 - 60.75*1.36335) / (60.75*1.36335)
        tr_US05 = (52.64 - 51.57) / 51.57
        tr_US60 = (35.4 - 35.2) / 35.2
        self.assertEqual(
            1. + (tr_BE*0.275706352908 + tr_DE*0.0428412771861 + tr_US05*0.303424689127 + tr_US60*0.193368025181),
            self.portfolio.calculate_total_performance(start, end).iloc[-1]
        )


if __name__ == '__main__':
    unittest.main()
